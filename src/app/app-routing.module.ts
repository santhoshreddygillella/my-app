import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageIdentityComponent } from './manage-identity/manage-identity.component';


const routes: Routes = [
  {path : 'manageIdentity', component :  ManageIdentityComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
